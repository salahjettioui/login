<?php
session_start();
include'header.php';
include("controller.php");
if(isset($_POST["submit"])){
    if(login($_POST["name"],$_POST["password"],$_POST["email"])){
        header("location:home.php");
    }
}
?>
<div class="container">
        <form action="login.php" method="POST" class="form" id="login">
            <h1 class="form__title">Login</h1>
            <div class="form__input-group">
                <input type="text" class="form__input" autofocus placeholder="Username" name="name">
            </div>
            <div class="form__input-group">
                <input type="email" class="form__input" autofocus placeholder="email" name="email">
            </div>
            <div class="form__input-group">
                <input type="password" class="form__input" autofocus placeholder="Password" name="password">
            </div>
            <button class="form__button" type="submit" name="submit">Continue</button>
            <p class="form__text">
                <a class="form__link" href="signin.php" id="linkCreateAccount" >Don't have an account? Create account</a>
            </p>
        </form>
</div>
        </body>
</html>

